# Awesome Game App

Hello AGA is information app about video games.

## Tech Stack

 1. MVVM
 2. Dagger 2
 3. Retrofit
 4. Coroutines
 5. Paging
 6. Navigation Component
 7. Kotlin

## WHY MVVM?

We have a lot of design patterns. MVC, MVP, MVVM , MVI.
MVC: Hard to scale, difficult to manage and also it is not good with separation of concerns principle.
MVP: It is good with SoC. But presenter layer knows the view. It is not good for business logic. And also MVP design use a lot of ping-pong between Presenter and View. Yes we can add Observable pattern but, It will be bit much work. Because we need to add some customized lifecycle components.
**MVVM:** Presenter layer does not about views. We can use same view models with different views. Also ViewModel class aware about view's lifecycle. It's so efficient for usage of resources.

## Dependency Injection Architecture

Common mistake with dagger, people using modules with singleton(Which is application scope). But it is poor decision usage of resource. Application scope should have expensive objects to create. For example Retrofit, OkHttp, Room..

We can create scopes with annotations. In this project we have FragmentScope

    @Scope
    @MustBeDocumented
    @Retention(value = AnnotationRetention.RUNTIME)
    annotation class FragmentScope

That means, objects with this name will be created when needed. This annotation should be use components and instances. For example Splash :

SplashComponent.kt


    @FragmentScope
	@Subcomponent(modules = [SplashModule::class])
	interface SplashComponent {

    @Subcomponent.Factory
    interface Factory{
        fun create():SplashComponent
    }

    fun inject(fragment: SplashFragment)
	}

SplashViewModule.kt

    @Module(
    includes = [
        SplashModule.ProvideViewModel::class
    ]
    )
	abstract class SplashModule {

    @Module
	class ProvideViewModel {

      @Provides
	  @IntoMap
	  @ViewModelKey(SplashViewModel::class)
        fun provideViewModel(): ViewModel =  SplashViewModel()
    }

    @Module
		class InjectViewModel {

	      @Provides
		  fun provideViewModel(
            factory: ViewModelProvider.Factory,
			viewModelStore: ViewModelStore ): SplashViewModel =
		           ViewModelProvider(viewModelStore, factory).get(SplashViewModel::class.java)
	    }
	}

SplashFragment.kt

    @Inject
	lateinit var splashViewModel: SplashViewModel

	override fun onAttach(context: Context) {
	    super.onAttach(context)
	    getAppComponent().splashComponent().create().inject(this)
	}

When fragment attached, we are creating ViewModel instance. Because of this why scope name is FragmentScope.

PS: I don't use AndroidInjector way. I prefer traditional way. Because i feel, i have more control about dagger. (Less magic :))



## Paging

(To be honest, this is the first time i use this component. But i liked it so much. Also i research about best practices. This implementation similar to google sample.)  Paging component use data source (with factory pattern), observable pattern (for observe data changes-provide LivePagedList< T >) . Paging implementation has some steps.

Provide DataSource

GameDataSource.kt

    class GamesDataSource @Inject constructor(
    private val gameService: GameService
	    //PageKeyedDataSource: This is right choice for games endpoint
	    //https://...page_size=10&page=1 this endpoint needs page number
	    //There are two more DataSource types
	) :  PageKeyedDataSource<Int, GameUIModel>(), CoroutineScope {

	// This method triggerd when there is no data at the list
	override fun loadInitial(
		params: LoadInitialParams<Int>,
	    callback: LoadInitialCallback<Int, GameUIModel>)	 {
		    val currentPage = 1
			val nextPage = currentPage + 1
			//Get response from endpoint
		    val list = request(params.requestedLoadSize, currentPage)

			//send data and nextPage number
			callback.onResult(list, null, nextPage)
	}
    override fun loadAfter(
	    params: LoadParams<Int>,
        callback: LoadCallback<Int, GameUIModel>) {
            val currentPage = params.key
		    val nextPage = currentPage + 1
	        //Get response from endpoint
		    val list = request(params.requestedLoadSize, currentPage)

		    //send data and nextPage number
		    callback.onResult(list, nextPage)
	}

Provide DataSourceFactory

    class GameDataFactory @Inject constructor(
    private val gamesDataSource: GamesDataSource) :
    DataSource.Factory<Int, GameUIModel>() {
    // This live data provides network state from GameDataSource.kt
    val mutableGameDataSource: MutableLiveData<GamesDataSource> = MutableLiveData()
    private lateinit var _gamesDataSource: GamesDataSource

	    override fun create(): DataSource<Int, GameUIModel> {
	        _gamesDataSource = gamesDataSource
			mutableGameDataSource.postValue(_gamesDataSource)
	        return gamesDataSource
	  }

    }

DataSource should be create when ViewModel created.
Create an instance of DataSource with factory pattern.

HomeViewModel.kt

    @FragmentScope
	class HomeViewModel @Inject constructor(
    //Home repository is unused. But I will add some features. (Etc. room)
	 private val homeRepository: HomeRepository,
	 private val gameDataFactory: GameDataFactory,
	 // config Instance provide from DAgger
	 config: PagedList.Config
	) :
    BaseViewModel() {
	    //Observe gameDataFactory.mutableGameDataSource
	    private val networkState: LiveData<GamesDataSource.State> =
	        Transformations.switchMap(gameDataFactory.mutableGameDataSource) {
				 it.state
			}

	  fun networkState() = networkState

	  // Here is LivePagedListBuilder created
	  private val _gamesLiveData: LiveData<PagedList<GameUIModel>> =
        LivePagedListBuilder(gameDataFactory, config).build()

	  fun gamesLiveData() = _gamesLiveData

	..}

RecyclerView's adapter should be extend from PagedListAdapter

GameAdapter.kt

    class GameAdapter(private val itemClick: (game: GameUIModel) -> Unit) :
    PagedListAdapter<GameUIModel, GameAdapter.ViewHolder>(GameDiffCallback()) {

PS: DiffCallback class should be extend from DiffUtil.ItemCallback< T >()

Observe and display list

HomeFragment.kt

    homeViewModel.gamesLiveData().observe(viewLifecycleOwner, Observer {
		 it?.let {
		   //submitList is special method from PagedListAdapter
		   gameAdapter.submitList(it)
	     }
	})

## Coroutines & Retrofit

Retrofit supports suspending functions. Just add suspend keyword for retrofit service interface.

GameService.kt

    @GET("games/{id}")
	suspend fun getGame(@Path("id") id: Int): GameDetailResponseModel

Normally network calls, reading file/db , right dispatcher is Dispatcher.IO. But Retrofit uses own customized Dispatcher.  And also this is main safe dispatcher. That means it can be used from main thread. For example:

GameDataSource.kt

    private val job = Job()

	// Set Dispatchers.Main because retrofit is main-safe
	override val coroutineContext: CoroutineContext
    get() = job + Dispatchers.Main

	override fun loadInitial(
		params: LoadInitialParams<Int>,
	    callback: LoadInitialCallback<Int, GameUIModel> ) {

        val currentPage = 1
		val nextPage = currentPage + 1
		//launch keyword can be used this way
	    //because this class extends from CoroutineScope
	    launch {
		    //Used try-catch because of error handling (can be inprove)
			try {
	            state.value = State.LOADING
		        val list = request(params.requestedLoadSize, currentPage)
	            callback.onResult(list, null, nextPage)
	            state.value = State.INITIAL_SUCCESS
		    } catch (ex: Exception) {
	            state.value = State.FAIL
		    }

       }
    }

    private suspend fun request(pageSize: Int, page: Int): List<GameUIModel> { ..


## Q&A

 1. What should be the part of this app that needs more time to develop or improve?
 Generic Error Handling
 Room Implementation (Single source of truth. )
 2. Ready to store?
 Technically, yes. Design is not good. World is not read for my design:)
 3.  Which part did you like most in this app?
 Pagination part. I did play with paging component. And I love it. Actually this is why i don't implement room. Because i was curious about performance with just in-memory data. I think it is good.





