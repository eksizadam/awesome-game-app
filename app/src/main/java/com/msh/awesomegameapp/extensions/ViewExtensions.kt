package com.msh.awesomegameapp.extensions

import android.content.Context
import android.os.Build
import android.text.Html
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView

fun Context.toast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun AppCompatTextView.setHtmlText(text: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        this.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
    } else {
        this.text = Html.fromHtml(text);
    }
}