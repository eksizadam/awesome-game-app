package com.msh.awesomegameapp.di.components

import com.msh.awesomegameapp.ui.game.GameComponent
import com.msh.awesomegameapp.ui.home.HomeComponent
import com.msh.awesomegameapp.ui.splash.SplashComponent
import dagger.Module

@Module(
    subcomponents = [
        HomeComponent::class,
        SplashComponent::class,
        GameComponent::class
    ]
)
class AppSubComponents {
}