package com.msh.awesomegameapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.msh.awesomegameapp.ui.base.BaseViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Provider

@Module()
class UIModule {

    @Provides
    fun provideViewModelFactory(
        providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ): ViewModelProvider.Factory =
        BaseViewModelFactory(providers)

}