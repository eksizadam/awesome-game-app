package com.msh.awesomegameapp.di

import android.content.Context
import com.msh.awesomegameapp.AwesomeGameApp
import com.msh.awesomegameapp.di.components.AppSubComponents
import com.msh.awesomegameapp.di.module.NetworkModule
import com.msh.awesomegameapp.di.module.UIModule
import com.msh.awesomegameapp.ui.game.GameComponent
import com.msh.awesomegameapp.ui.home.HomeComponent
import com.msh.awesomegameapp.ui.splash.SplashComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        NetworkModule::class,
        UIModule::class,
        AppSubComponents::class
    ]
)

@Singleton
interface AppComponent {

    fun inject(app: AwesomeGameApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: AwesomeGameApp): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun splashComponent(): SplashComponent.Factory
    fun homeComponent(): HomeComponent.Factory
    fun gameComponent():GameComponent.Factory

}