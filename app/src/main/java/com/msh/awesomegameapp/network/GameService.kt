package com.msh.awesomegameapp.network

import com.msh.awesomegameapp.ui.game.model.network.GameDetailResponseModel
import com.msh.awesomegameapp.ui.home.model.network.GamesResponseModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GameService {

    @GET("games")
    suspend fun getGames(@Query("page_size") pageSize: Int, @Query("page") page: Int): GamesResponseModel

    @GET("games/{id}")
    suspend fun getGame(@Path("id") id: Int): GameDetailResponseModel
}