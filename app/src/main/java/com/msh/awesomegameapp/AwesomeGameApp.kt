package com.msh.awesomegameapp

import android.app.Application
import com.msh.awesomegameapp.di.AppComponent
import com.msh.awesomegameapp.di.DaggerAppComponent

class AwesomeGameApp : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .application(this)
            .context(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        appComponent.inject(this)
    }


}