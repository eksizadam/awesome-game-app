package com.msh.awesomegameapp.ui.splash

import com.msh.awesomegameapp.di.scope.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [SplashModule::class])
interface SplashComponent {

    @Subcomponent.Factory
    interface Factory{
        fun create():SplashComponent
    }

    fun inject(fragment: SplashFragment)
}