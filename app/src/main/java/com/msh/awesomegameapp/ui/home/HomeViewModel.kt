package com.msh.awesomegameapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.msh.awesomegameapp.di.scope.FragmentScope
import com.msh.awesomegameapp.ui.base.BaseViewModel
import com.msh.awesomegameapp.ui.home.list.GameDataFactory
import com.msh.awesomegameapp.ui.home.list.GamesDataSource
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import javax.inject.Inject

@FragmentScope
class HomeViewModel @Inject constructor(
    //Home repository is unused. But I will add some features. (Etc. room)
    private val homeRepository: HomeRepository,
    private val gameDataFactory: GameDataFactory,
    config: PagedList.Config
) :
    BaseViewModel() {

    private val networkState: LiveData<GamesDataSource.State> =
        Transformations.switchMap(gameDataFactory.mutableGameDataSource) {
            it.state
        }



    fun networkState() = networkState

    private val _gamesLiveData: LiveData<PagedList<GameUIModel>> =
        LivePagedListBuilder(gameDataFactory, config).build()

    fun gamesLiveData() = _gamesLiveData



    override fun onCleared() {
        super.onCleared()
        gameDataFactory.mutableGameDataSource.value?.clearJobs()
    }

}