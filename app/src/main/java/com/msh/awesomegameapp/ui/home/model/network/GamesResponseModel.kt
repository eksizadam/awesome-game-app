package com.msh.awesomegameapp.ui.home.model.network

import com.google.gson.annotations.SerializedName

data class GamesResponseModel (
    val count:Int,
    val next:String,
    @SerializedName("results")
    val games:List<GameResponseModel>
)