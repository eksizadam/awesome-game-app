package com.msh.awesomegameapp.ui.splash

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.msh.awesomegameapp.R
import com.msh.awesomegameapp.ui.base.BaseFragment
import javax.inject.Inject

class SplashFragment : BaseFragment() {

    @Inject
    lateinit var splashViewModel: SplashViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getAppComponent().splashComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_splash, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashViewModel.startDelay()

        splashViewModel.state.observe(viewLifecycleOwner, Observer {

            if (it == SplashViewModel.SplashState.FINISH) {
                navController.navigate(R.id.splash_to_home)
            }

        })

    }

}