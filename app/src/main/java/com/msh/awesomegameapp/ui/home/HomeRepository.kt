package com.msh.awesomegameapp.ui.home

import androidx.lifecycle.MutableLiveData
import com.msh.awesomegameapp.ui.base.BaseRepository
import com.msh.awesomegameapp.ui.home.list.GamesDataSource
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val gamesDataSource: GamesDataSource
) : BaseRepository() {


    fun clearAllJobs(){
        gamesDataSource.clearJobs()
    }

}