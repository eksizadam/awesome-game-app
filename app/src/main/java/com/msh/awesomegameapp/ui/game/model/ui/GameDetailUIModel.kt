package com.msh.awesomegameapp.ui.game.model.ui

data class GameDetailUIModel(
    val id: Int,
    val name: String,
    val description: String,
    val metacritic: Int,
    val releaseDate: String,
    val image: String,
    val webSite: String,
    val rating: Double,
    val topRating: Int,
    val redditUrl: String,
    val platforms: List<String>
)