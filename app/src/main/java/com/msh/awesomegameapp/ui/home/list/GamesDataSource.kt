package com.msh.awesomegameapp.ui.home.list

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.msh.awesomegameapp.network.GameService
import com.msh.awesomegameapp.ui.home.model.network.PlatformResponseModel
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class GamesDataSource @Inject constructor(
    private val gameService: GameService
) :
    PageKeyedDataSource<Int, GameUIModel>(), CoroutineScope {

    private val job = Job()

    // Set Dispatchers.Main because retrofit is main-safe
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    val state: MutableLiveData<State> = MutableLiveData()


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, GameUIModel>
    ) {
        val currentPage = 1
        val nextPage = currentPage + 1

        launch {

            try {
                state.value = State.LOADING

                val list = request(params.requestedLoadSize, currentPage)

                callback.onResult(list, null, nextPage)

                state.value = State.INITIAL_SUCCESS

            } catch (ex: Exception) {
                state.value = State.FAIL
            }

        }
    }

    private suspend fun convertPlatform(list: List<PlatformResponseModel>): List<String> =
        withContext(Dispatchers.Default) {
            list.map {
                it.platform.name
            }
        }


    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GameUIModel>
    ) {
        val currentPage = params.key
        val nextPage = currentPage + 1

        launch {
            try {

                state.value = State.PAGING

                val list = request(params.requestedLoadSize, currentPage)
                callback.onResult(list, nextPage)

                state.value = State.PAGING_SUCCESS

            } catch (ex: Exception) {
                state.value = State.FAIL
            }
        }

    }

    private suspend fun request(pageSize: Int, page: Int): List<GameUIModel> {
        val response = gameService.getGames(pageSize, page).games

        return withContext(Dispatchers.Default) {
            response.map {
                GameUIModel(
                    id = it.id,
                    name = it.name,
                    releaseDate = it.released,
                    image = it.backgroundImage,
                    rating = it.rating,
                    ratingTop = it.ratingTop,
                    platforms = convertPlatform(it.platforms)
                )
            }
        }
    }

    fun clearJobs() {
        job.cancel()
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GameUIModel>
    ) {
    }

    enum class State {
        LOADING,
        INITIAL_SUCCESS,
        PAGING,
        PAGING_SUCCESS,
        FAIL
    }


}