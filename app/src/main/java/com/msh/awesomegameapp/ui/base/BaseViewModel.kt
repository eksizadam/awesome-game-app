package com.msh.awesomegameapp.ui.base

import androidx.lifecycle.ViewModel
import com.msh.awesomegameapp.ui.home.model.network.PlatformResponseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class BaseViewModel:ViewModel() {

    protected suspend fun convertPlatform(list: List<PlatformResponseModel>): List<String> =
        withContext(Dispatchers.Default) {
            list.map {
                it.platform.name
            }
        }

}