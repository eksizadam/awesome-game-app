package com.msh.awesomegameapp.ui.game

import com.msh.awesomegameapp.network.GameService
import com.msh.awesomegameapp.ui.base.BaseRepository
import com.msh.awesomegameapp.ui.game.model.network.GameDetailResponseModel
import javax.inject.Inject

class GameRepository @Inject constructor(
    private val gameService: GameService
) : BaseRepository() {

    suspend fun fetchGame(id: Int): GameDetailResponseModel {
        return gameService.getGame(id)
    }

}