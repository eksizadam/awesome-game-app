package com.msh.awesomegameapp.ui.game.model.network

import com.google.gson.annotations.SerializedName
import com.msh.awesomegameapp.ui.game.model.network.TagResponseModel
import com.msh.awesomegameapp.ui.home.model.network.PlatformResponseModel

data class GameDetailResponseModel(
    val id: Int,
    val name: String,
    val description: String,
    val metacritic: Int,
    val released: String,
    @SerializedName("background_image")
    val backgroundImage: String,
    @SerializedName("website")
    val webSite: String,
    val rating: Double,
    @SerializedName("rating_top")
    val topRating: Int,
    @SerializedName("reddit_url")
    val redditUrl: String,
    @SerializedName("parent_platforms")
    val platforms: List<PlatformResponseModel>,
    val tags: List<TagResponseModel>
)