package com.msh.awesomegameapp.ui.home.model.network


data class PlatformResponseModel(
    val platform:Platform
)

data class Platform(
    val id: Int,
    val name: String,
    val slug: String
)
