package com.msh.awesomegameapp.ui.base

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.msh.awesomegameapp.AwesomeGameApp
import com.msh.awesomegameapp.di.AppComponent
import com.msh.awesomegameapp.extensions.toast

abstract class BaseFragment : Fragment() {

    lateinit var navController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
    }

    fun getAppComponent(): AppComponent = (activity?.application as AwesomeGameApp).appComponent

    fun showToast(message: String) {
        context?.toast(message)
    }


    fun showProgressBar(progressBar: ProgressBar) {
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgressBar(progressBar: ProgressBar) {
        progressBar.visibility = View.GONE
    }

}