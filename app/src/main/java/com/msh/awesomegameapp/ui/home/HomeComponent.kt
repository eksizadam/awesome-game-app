package com.msh.awesomegameapp.ui.home

import com.msh.awesomegameapp.di.scope.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [HomeModule::class])
interface HomeComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): HomeComponent
    }

    fun inject(fragment: HomeFragment)

}