package com.msh.awesomegameapp.ui.home.model.network

import com.google.gson.annotations.SerializedName

data class GameResponseModel(
    val id: Int,
    val slug: String,
    val name: String,
    val released: String?,
    @SerializedName("background_image")
    val backgroundImage: String,
    val rating: Double,
    @SerializedName("rating_top")
    val ratingTop: Int,
    @SerializedName("parent_platforms")
    val platforms: List<PlatformResponseModel>
)