package com.msh.awesomegameapp.ui.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.msh.awesomegameapp.di.ViewModelKey
import com.msh.awesomegameapp.network.GameService
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module(
    includes = [
        GameModule.ProvideViewModel::class
    ]
)
abstract class GameModule {

    @Module
    class ProvideViewModel {

        @Provides
        fun provideRepository(
            gameService: GameService
        ): GameRepository = GameRepository(gameService)

        @Provides
        @IntoMap
        @ViewModelKey(GameViewModel::class)
        fun provideViewModel(
            gameRepository: GameRepository
        ): ViewModel =
            GameViewModel(gameRepository)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            viewModelStore: ViewModelStore
        ): GameViewModel = ViewModelProvider(viewModelStore, factory).get(GameViewModel::class.java)
    }

}