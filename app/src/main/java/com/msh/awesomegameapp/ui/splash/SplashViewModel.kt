package com.msh.awesomegameapp.ui.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.msh.awesomegameapp.di.scope.FragmentScope
import com.msh.awesomegameapp.ui.base.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@FragmentScope
class SplashViewModel @Inject constructor(): BaseViewModel() {

    private val _state = MutableLiveData<SplashState>()
    val state = _state

    init {
        _state.value = SplashState.WAITING
    }

    fun startDelay(){
        viewModelScope.launch {
            delay(2_000)
            state.value = SplashState.FINISH
        }
    }


    enum class SplashState {
        WAITING,
        FINISH
    }

}