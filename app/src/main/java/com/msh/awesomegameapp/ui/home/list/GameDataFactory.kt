package com.msh.awesomegameapp.ui.home.list

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import javax.inject.Inject

class GameDataFactory @Inject constructor(private val gamesDataSource: GamesDataSource) :
    DataSource.Factory<Int, GameUIModel>() {

    val mutableGameDataSource: MutableLiveData<GamesDataSource> = MutableLiveData()
    private lateinit var _gamesDataSource: GamesDataSource

    override fun create(): DataSource<Int, GameUIModel> {
        _gamesDataSource = gamesDataSource
        mutableGameDataSource.postValue(_gamesDataSource)
        return gamesDataSource
    }
}