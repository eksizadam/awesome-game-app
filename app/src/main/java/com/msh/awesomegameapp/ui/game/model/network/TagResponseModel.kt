package com.msh.awesomegameapp.ui.game.model.network

import com.google.gson.annotations.SerializedName

data class TagResponseModel(
    val id: Int,
    val name: String,
    val slug: String,
    val language: String,
    @SerializedName("games_count")
    val gamesCount: Int,
    @SerializedName("image_background")
    val image: String
)