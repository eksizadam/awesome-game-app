package com.msh.awesomegameapp.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.msh.awesomegameapp.R
import com.msh.awesomegameapp.ui.base.BaseFragment
import com.msh.awesomegameapp.ui.base.BaseRepository
import com.msh.awesomegameapp.ui.game.GameFragment.Companion.GAME
import com.msh.awesomegameapp.ui.home.list.GameAdapter
import com.msh.awesomegameapp.ui.home.list.GamesDataSource
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment() {

    @Inject
    lateinit var homeViewModel: HomeViewModel

    private val gameAdapter: GameAdapter = GameAdapter { game ->
        navController.navigate(R.id.home_to_game, bundleOf(GAME to game))
    }
    private lateinit var layoutManager: LinearLayoutManager

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getAppComponent().homeComponent().create().inject(this)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initGameRecyclerView()
        observeData()
    }

    private fun observeData() {

        homeViewModel.gamesLiveData().observe(viewLifecycleOwner, Observer {
            it?.let {
                gameAdapter.submitList(it)
            }
        })



        homeViewModel.networkState().observe(viewLifecycleOwner, Observer { state ->

            state?.let {

                when (it) {
                    GamesDataSource.State.LOADING -> showProgressBar(loadingProgressBar)

                    GamesDataSource.State.INITIAL_SUCCESS -> hideProgressBar(loadingProgressBar)
                    GamesDataSource.State.PAGING -> {
                        // Do nothing
                    }
                    GamesDataSource.State.PAGING_SUCCESS -> {
                        // Do nothing
                    }
                    GamesDataSource.State.FAIL -> {
                        hideProgressBar(loadingProgressBar)
                        showToast("Something went wrong .. ")
                    }
                }
            }

        })


    }


    private fun initGameRecyclerView() {

        layoutManager = LinearLayoutManager(context)
        gameRecyclerView.layoutManager = layoutManager
        gameRecyclerView.adapter = gameAdapter
    }


}