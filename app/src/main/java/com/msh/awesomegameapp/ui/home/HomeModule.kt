package com.msh.awesomegameapp.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.msh.awesomegameapp.di.ViewModelKey
import com.msh.awesomegameapp.network.GameService
import com.msh.awesomegameapp.ui.home.list.GameDataFactory
import com.msh.awesomegameapp.ui.home.list.GamesDataSource
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module(
    includes = [
        HomeModule.ProvideViewModel::class
    ]
)
abstract class HomeModule {

    companion object {
        const val PAGE_SIZE = 30
    }

    @Module
    class ProvideViewModel {


        @Provides
        fun providePagedListConfig(): PagedList.Config = PagedList.Config.Builder()
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPageSize(PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()


        @Provides
        fun provideDataSource(
            gameService: GameService
        ): GamesDataSource = GamesDataSource(gameService)

        @Provides
        fun provideRepository(gamesDataSource: GamesDataSource): HomeRepository =
            HomeRepository(gamesDataSource)

        @Provides
        @IntoMap
        @ViewModelKey(HomeViewModel::class)
        fun provideViewModel(
            config: PagedList.Config,
            homeRepository: HomeRepository,
            homeDataFactory: GameDataFactory
        ): ViewModel =
            HomeViewModel(homeRepository, homeDataFactory, config)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            viewModelStore: ViewModelStore
        ): HomeViewModel = ViewModelProvider(viewModelStore, factory).get(HomeViewModel::class.java)
    }

}