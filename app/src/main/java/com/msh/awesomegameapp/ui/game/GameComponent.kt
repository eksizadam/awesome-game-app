package com.msh.awesomegameapp.ui.game

import com.msh.awesomegameapp.di.scope.FragmentScope
import dagger.Subcomponent


@FragmentScope
@Subcomponent(modules = [GameModule::class])
interface GameComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): GameComponent
    }

    fun inject(fragment: GameFragment)

}