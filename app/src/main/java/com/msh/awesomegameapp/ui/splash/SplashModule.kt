package com.msh.awesomegameapp.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.msh.awesomegameapp.di.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module(
    includes = [
        SplashModule.ProvideViewModel::class
    ]
)
abstract class SplashModule {

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(SplashViewModel::class)
        fun provideViewModel(
        ): ViewModel =
            SplashViewModel()
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            viewModelStore: ViewModelStore
        ): SplashViewModel =
            ViewModelProvider(viewModelStore, factory).get(SplashViewModel::class.java)
    }

}