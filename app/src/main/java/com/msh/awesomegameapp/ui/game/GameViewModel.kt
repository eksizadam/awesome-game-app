package com.msh.awesomegameapp.ui.game

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.msh.awesomegameapp.di.scope.FragmentScope
import com.msh.awesomegameapp.ui.base.BaseViewModel
import com.msh.awesomegameapp.ui.game.model.network.TagResponseModel
import com.msh.awesomegameapp.ui.game.model.ui.GameDetailUIModel
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@FragmentScope
class GameViewModel @Inject constructor(
    private val gameRepository: GameRepository
) :
    BaseViewModel() {

    private val _game = MutableLiveData<GameUIModel>()
    val game = _game

    private val _gameDetail = MutableLiveData<GameDetailUIModel>()
    val gameDetail = _gameDetail

    fun setGame(gameUIModel: GameUIModel) {
        _game.value = gameUIModel
    }

    fun getGameDetail(id: Int) {
        viewModelScope.launch {
            val response = gameRepository.fetchGame(id)

            gameDetail.value = GameDetailUIModel(
                id = response.id,
                name = response.name,
                description = response.description,
                metacritic = response.metacritic,
                releaseDate = response.released,
                image = response.backgroundImage,
                webSite = response.webSite,
                rating = response.rating,
                topRating = response.topRating,
                redditUrl = response.redditUrl,
                platforms = convertPlatform(response.platforms)
            )
        }
    }


}