package com.msh.awesomegameapp.ui.game

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.msh.awesomegameapp.R
import com.msh.awesomegameapp.extensions.setHtmlText
import com.msh.awesomegameapp.ui.base.BaseFragment
import com.msh.awesomegameapp.ui.game.model.ui.GameDetailUIModel
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_game.*
import javax.inject.Inject


class GameFragment : BaseFragment() {

    companion object {
        const val GAME = "game"
    }

    @Inject
    lateinit var gameViewModel: GameViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getAppComponent().gameComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_game, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleArgs()
        observeData()

    }

    private fun observeData() {
        gameViewModel.game.observe(viewLifecycleOwner, Observer {

            if (it != null) {
                loadBaseGame(it)
            }

        })

        gameViewModel.gameDetail.observe(viewLifecycleOwner, Observer {

            if (it != null) {
                loadDetailedGame(it)
            }

        })
    }

    private fun handleArgs() {
        arguments?.get(GAME)?.let {
            it as GameUIModel

            gameViewModel.getGameDetail(it.id)
            gameViewModel.setGame(it)
        }
    }

    private fun loadDetailedGame(gameDetailUIModel: GameDetailUIModel) {
        Picasso.get().load(gameDetailUIModel.image).fit().into(gameImageView)

        gameNameText.text = gameDetailUIModel.name
        releaseDateText.text = gameDetailUIModel.releaseDate
        ratingText.text = gameDetailUIModel.rating.toString()
        topRatingText.text = gameDetailUIModel.topRating.toString()
        platformsText.text = gameDetailUIModel.platforms.joinToString()
        metacriticText.text = gameDetailUIModel.metacritic.toString()
        redditUrlText.text = gameDetailUIModel.redditUrl
        webSiteText.text = gameDetailUIModel.webSite

        descriptionText.setHtmlText(gameDetailUIModel.description)
    }

    private fun loadBaseGame(gameUIModel: GameUIModel) {
        Picasso.get().load(gameUIModel.image).fit().into(gameImageView)

        gameNameText.text = gameUIModel.name
        releaseDateText.text = gameUIModel.releaseDate
        ratingText.text = gameUIModel.rating.toString()
        topRatingText.text = gameUIModel.ratingTop.toString()
        platformsText.text = gameUIModel.platforms.joinToString()
    }


}

