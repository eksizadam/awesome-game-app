package com.msh.awesomegameapp.ui.home.model.ui

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameUIModel(
    val id: Int,
    val name: String,
    val releaseDate: String?,
    val image: String,
    val rating: Double,
    val ratingTop: Int,
    val platforms: List<String>
) : Parcelable {
    override fun equals(other: Any?): Boolean {

        if (javaClass != other?.javaClass) {
            return false
        }

        other as GameUIModel

        if (id != other.id) {
            return false
        }
        if (name != other.name) {
            return false
        }
        if (releaseDate != other.releaseDate) {
            return false
        }
        if (image != other.image) {
            return false
        }
        if (rating != other.rating) {
            return false
        }
        if (ratingTop != other.ratingTop) {
            return false
        }

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + name.hashCode()
        result = 31 * result + releaseDate.hashCode()
        result = 31 * result + image.hashCode()
        result = 31 * result + rating.hashCode()
        result = 31 * result + ratingTop
        return result
    }
}