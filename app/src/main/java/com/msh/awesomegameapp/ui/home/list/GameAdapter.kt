package com.msh.awesomegameapp.ui.home.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.msh.awesomegameapp.R
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_game.view.*

class GameAdapter(private val itemClick: (game: GameUIModel) -> Unit) :
    PagedListAdapter<GameUIModel, GameAdapter.ViewHolder>(GameDiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_game, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, itemClick)
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image = itemView.gameImageView
        private val name = itemView.gameNameText
        private val releaseDate = itemView.releaseDateText
        private val rating = itemView.ratingText
        private val topRating = itemView.topRatingText


        fun bind(
            game: GameUIModel,
            itemClick: (game: GameUIModel) -> Unit
        ) {

            Picasso.get().load(game.image).fit().into(image)
            name.text = game.name
            releaseDate.text = game.releaseDate
            rating.text = game.rating.toString()
            topRating.text = game.ratingTop.toString()

            itemView.setOnClickListener {
                itemClick(game)
            }

        }

    }

}