package com.msh.awesomegameapp.ui.home.list

import androidx.recyclerview.widget.DiffUtil
import com.msh.awesomegameapp.ui.home.model.ui.GameUIModel

class GameDiffCallback(
) : DiffUtil.ItemCallback<GameUIModel>() {
    override fun areItemsTheSame(oldItem: GameUIModel, newItem: GameUIModel): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: GameUIModel, newItem: GameUIModel): Boolean =
        oldItem == newItem

}